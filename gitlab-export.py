import gitlab
import time
import os

gl = gitlab.Gitlab('https://gitlab.ical.tw', 'your-private-token')

project = gl.projects.list(all=True, owned=True, as_list=False)
for p in project:
    print('Exporting', p.path_with_namespace)
    export = p.exports.create()
    export.refresh()

    while export.export_status != 'finished':
        time.sleep(1)
        export.refresh()

    os.makedirs(p.namespace['path'], exist_ok=True)
    print('makedirs', p.namespace['path'])
    with open(p.path_with_namespace + '.tgz', 'wb') as f:
        print('write', p.path_with_namespace + '.tgz')
        export.download(streamed=True, action=f.write)

