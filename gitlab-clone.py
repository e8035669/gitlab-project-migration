import gitlab
import time
import os
import subprocess

gl = gitlab.Gitlab('https://gitlab.ical.tw', 'your-private-token')

project = gl.projects.list(all=True, owned=True, as_list=False)
for p in project:
    print('Exporting', p.path_with_namespace)

    basedir = 'clone'
    dirname = os.path.join(basedir, p.namespace['path'])
    os.makedirs(dirname, exist_ok=True)
    print('makedirs', p.namespace['path'])

    project_dir = os.path.join(basedir, p.path_with_namespace)

    print('remove', project_dir)
    if os.path.exists(project_dir):
        subprocess.call(['rm', '-rf', project_dir])

    git_url = p.ssh_url_to_repo
    subprocess.call(['git', 'clone', git_url, project_dir])

