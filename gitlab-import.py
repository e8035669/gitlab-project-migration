import gitlab
import glob
import os
from pathlib import PurePath
import time


gl = gitlab.Gitlab('https://gitlab.com', 'your-private-token')

all_proj = gl.projects.list(all=True, owned=True)
exist_proj_names = list(map(lambda p: p.path, all_proj))
print('exist project', exist_proj_names)

paths = glob.glob('export/*/*.tgz')
for p in paths:
    p = PurePath(p)
    print('file:', str(p), 'name:', p.stem)

    if p.stem in exist_proj_names:
        print('Project exists, skip')
        continue

    for attempt in range(5):
        try:
            with open(p, 'rb') as f:
                result = gl.projects.import_project(f, p.stem)

            print('Wait for import')
            proj = gl.projects.get(result['id'])

            while proj.import_status == 'started':
                time.sleep(1)
                proj = gl.projects.get(proj.id)
            print('Import done')
            break
        except Exception as e:
            print(e)
            print('retrying', attempt)


